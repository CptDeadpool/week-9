﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Scorekeeper : MonoBehaviour {

	public int scorePerGoal = 1;
	public int maxScore = 2;
	private int[] score = new int[2];
	public Text[] scoreText;
	//private bool gameOver;
	//private bool restart;
	public Text winText;

	// Use this for initialization
	void Start () {
		//gameOver = false;
		//restart = false;

		//subscribe to events from all the goals
		Goal[] goals = FindObjectsOfType<Goal> ();

		for (int i = 0; i < goals.Length; i++) {
			goals [i].scoreGoalEvent += OnScoreGoal;
		}

		//reset scores to zero
		for (int i = 0; i < score.Length; i++) {
			score [i] = 0;
			scoreText [i].text = "0";
		}
	}

	public void OnScoreGoal(int player) {
		//add points to the player whose goal it is
		score[player] += scorePerGoal;
		scoreText [player].text = score [player].ToString ();
		Debug.Log ("Player " + player + ": " + score [player]);

		if (score [0] >= maxScore) {
			winText.text = "Player 1 wins";
			Time.timeScale = 0.0f;
		} else if (score [1] >= maxScore) {
			winText.text = "Player 2 wins";
			Time.timeScale = 0.0f;
		}
	}

	//public void GameOver(int player)
	//{
		//gameOverText.text = "Player " + player + " wins!";
		//gameOver = true;
	//}
	
	// Update is called once per frame
	void Update () {
		
	}
}
