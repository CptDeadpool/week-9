﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {

	public AudioClip scoreClip;
	private AudioSource audio;
	public delegate void ScoreGoalHandler (int player);
	public ScoreGoalHandler scoreGoalEvent;
	public int player;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider collider) {
		//play score sound
		audio.PlayOneShot(scoreClip);

		//reset the puck to its starting position
		PuckControl puck = collider.gameObject.GetComponent<PuckControl> ();
		puck.ResetPosition();
		//notify event handles if there are any
		if (scoreGoalEvent != null) {
			scoreGoalEvent (player);
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
