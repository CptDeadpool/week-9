﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {


	public Transform startingPos;
	public bool usingKeys = true;
	public float speed = 20f;
	private Rigidbody rigidbody;
	private Vector3 GetMousePosition() {
		//create a ray from the camera
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		//find out where the reay intersects the XY plane
		Plane plane = new Plane (Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint (distance);
	}

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
		ResetPosition();
	}
	public void ResetPosition() {
		//teleport to the starting position
		rigidbody.MovePosition(startingPos.position);
		//stop it from moving
		rigidbody.velocity = Vector3.zero;
	}

	void FixedUpdate () {

		if (!usingKeys) {
			Vector3 pos = GetMousePosition ();
			Vector3 dir = pos - rigidbody.position;
			Vector3 vel = dir.normalized * speed;

			//check if this speed is going to overshoot target
			float move = speed * Time.fixedDeltaTime;
			float distToTarget = dir.magnitude;

			if (move > distToTarget) {
				//scale the velocity down appropriately
				vel = vel * distToTarget / move;
			}

			rigidbody.velocity = vel;
		} else {
			Vector3 dir = Vector3.zero;
			dir.x = Input.GetAxis ("Horizontal");
			dir.y = Input.GetAxis ("Vertical");

			rigidbody.velocity = dir * speed; 
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void onDrawGizmos() {
		//draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);
	}
}
