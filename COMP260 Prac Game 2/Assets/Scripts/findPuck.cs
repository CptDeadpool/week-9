﻿using UnityEngine;
using System.Collections;

public class findPuck : MonoBehaviour {

	Transform puck;
	Transform ai;
	public float speed;
	
	// Update is called once per frame
	void Update () {
		speed = Time.deltaTime * 30;
		puck = GameObject.FindGameObjectWithTag ("Puck").transform;
		ai = GameObject.FindGameObjectWithTag ("Enemy").transform;

		if (puck.position.y > 0) {
			ai.position = new Vector3 (ai.position.x, puck.position.y, 0);
		} else if (puck.position.y < 0) {
			ai.position = new Vector3 (ai.position.x, puck.position.y, 0);
		}
	}

	void OnCollisionEnter(Collision collision){
		if (collision.gameObject.name == "Puck"){
			collision.rigidbody.AddForce(-1000,0,0);
		}
	}
}
